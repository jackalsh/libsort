
#include <cstdlib>

#include "src/utils/utils.h"
#include "src/tests/Tester.h"

#include "src/core/BubbleSort.h"
#include "src/core/InsertionSort.h"
#include "src/core/QuickSort.h"
#include "src/core/HeapSort.h"
#include "src/core/RandomQuickSort.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    utils::DataGenerator generator;
    generator.setSize(100);
    generator.setMaxValue(10000);
    
    core::RandomQuickSort<int> sorter(generator.randomMT64());

    std::cout << std::endl << "Before" << std::endl;
    cout << sorter.result();
    std::cout << "(" << tests::isSorted<int>(sorter) << ")";
    sorter.sort();
    std::cout << std::endl << "After" << std::endl;
    cout << sorter.result() << std::endl;
    std::cout << "(" << tests::isSorted<int>(sorter) << ")";
    
    std::cout <<  sorter.info() << std::endl;
    std::cout << "Duration:" << sorter.duration() << "ms" << std::endl;
    
    tests::isSorted<int>(sorter);
    
    return 0;
}