
#ifndef BASESORT_H
#define	BASESORT_H

#include <iostream>
#include <vector>
#include <chrono>
#include <string>

#include <assert.h>

namespace core {
    template <typename T>
    using Compare = bool (*)(T, T);
    
    template <typename T>
    class BaseSort {
    public:
        BaseSort(const std::vector<T>& data);
        BaseSort(const BaseSort& orig);
        virtual ~BaseSort();
        
        void sort();
        const std::vector<T> &result() const;
        unsigned int duration() const; // ms
        virtual const std::string info() const = 0;
        
        void setCompare(Compare<T> compare);
        Compare<T> compare() const;
    protected:
        std::vector<T> &array();
        virtual void sort_algorithm() = 0; 
    private:
        std::vector<T> arrayData;
        unsigned int durationData;
        Compare<T> mCompare;
    };

    template<typename T>
    BaseSort<T>::BaseSort(const std::vector<T>& data)
        : arrayData(data),
          mCompare([](T first, T second) { return first < second; })
    {
        // empty
    }
    
    template<typename T>
    inline std::vector<T>& BaseSort<T>::array()
    {
        return arrayData;
    }
    
    template<typename T>
    unsigned int BaseSort<T>::duration() const
    {
        return durationData;
    }
    
    template<typename T>
    void BaseSort<T>::sort()
    {
        auto start = std::chrono::system_clock::now();
        sort_algorithm();
        auto end = std::chrono::system_clock::now();
        
        durationData = std::chrono::duration_cast<std::chrono::milliseconds>
                      (end-start).count();
        
    }
    
    template<typename T>
    inline const std::vector<T> &BaseSort<T>::result() const
    {
        return arrayData;
    }

    template<typename T>
    BaseSort<T>::BaseSort(const BaseSort& orig) {
    }

    template<typename T>
    BaseSort<T>::~BaseSort() {
    }
    
    template<typename T>
    void BaseSort<T>::setCompare(Compare<T> compare)
    {
        this->mCompare = compare;
    }
    
    template<typename T>
    Compare<T> BaseSort<T>::compare() const
    {
        return this->mCompare;
    }

}


#endif	/* BASESORT_H */

