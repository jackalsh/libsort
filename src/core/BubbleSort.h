
#ifndef BUBBLESORT_H
#define	BUBBLESORT_H

#include <utility>

#include "BaseSort.h"

namespace core {
    template<typename T>
    class BubbleSort : public BaseSort<T> {
    public:
        BubbleSort(const std::vector<T>& data);
        virtual const std::string info() const;
    protected:
        virtual void sort_algorithm();
    private:

    };
    
    template<typename T>
    const std::string BubbleSort<T>::info() const
    {
        return "Name: Bubble sort, a simple sorting algorithm that works by "
               "repeatedly stepping through the list to be sorted, comparing "
               "each pair of adjacent items and swapping them if they are in the wrong order\n"
               "Type: stable\n"
               "Worst case performance: O(n^2)\n"
               "Best case performance: O(n)\n"
               "Average case performance: O(n^2)\n"
               "Worst case space complexity: O(1) auxiliary\n";
    }

    template<typename T>
    BubbleSort<T>::BubbleSort(const std::vector<T>& data)
        : BaseSort<T>::BaseSort(data)
    {
        // empty
    }
    
    template<typename T>
    void BubbleSort<T>::sort_algorithm()
    {
        std::vector<T> &arr = BubbleSort<T>::array();
        for(unsigned int i = 0; i < arr.size() - 1; ++i)
            for(unsigned int j = 0; j < arr.size() - i - 1; ++j)
                if(BaseSort<T>::compare()(arr[j+1], arr[j]))
                    std::swap(arr[j],arr[j+1]);
    }

}

#endif	/* BUBBLESORT_H */

