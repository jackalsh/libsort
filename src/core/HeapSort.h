/* 
 * File:   HeapSort.h
 * Author: Olga
 *
 * Created on 18 октября 2014 г., 19:25
 */

#ifndef HEAPSORT_H
#define	HEAPSORT_H

#include <utility>

#include "BaseSort.h"

namespace core {
    template<typename T>
    class HeapSort : public BaseSort<T> {
    public:
        HeapSort(const std::vector<T>& data);
        virtual const std::string info() const;
    protected:
        virtual void sort_algorithm();
    private:
        inline int getHeapSize() const;
        
        inline int parent(const int index) const;
        inline int left(const int index) const;
        inline int right(const int index) const;
        
        void maxHeapify(const int index);
        void buildMaxHeap();
        
        int heap_size;
    };
    
    template<typename T>
    const std::string HeapSort<T>::info() const
    {
        return "Name: Heap sort";
    }

    template<typename T>
    HeapSort<T>::HeapSort(const std::vector<T>& data)
        : BaseSort<T>::BaseSort(data)
    {
        // empty
    }
    
    template<typename T>
    int HeapSort<T>::getHeapSize() const
    {
        return heap_size;
    }

    template<typename T>
    int HeapSort<T>::parent(const int index) const
    {
        return ((index + 1) / 2) - 1;
    }
    
    template<typename T>
    int HeapSort<T>::left(const int index) const
    {
        return index * 2 + 1;
    }
    
    template<typename T>
    int HeapSort<T>::right(const int index) const
    {
        return index * 2 + 2;
    }
    
    template<typename T>
    void HeapSort<T>::maxHeapify(const int index)
    {
        std::vector<T> &arr = HeapSort<T>::array();
        const int leftIndex = left(index);
        const int rightIndex = right(index);
        
        int largest = index;
        
        if( leftIndex < getHeapSize() &&  BaseSort<T>::compare()(arr[largest], arr[leftIndex]))
            largest = leftIndex;
        if( rightIndex < getHeapSize() && BaseSort<T>::compare()(arr[largest], arr[rightIndex]))
            largest = rightIndex;
        
        if(largest != index)
        {
            std::swap(arr[index], arr[largest]);
            maxHeapify(largest);
        }
    }
    
    template<typename T>
    void HeapSort<T>::buildMaxHeap()
    {
        heap_size = BaseSort<T>::array().size() - 1;
        
        for(int index = heap_size/2 ; index >= 0; --index)
            maxHeapify(index);
    }
    
    template<typename T>
    void HeapSort<T>::sort_algorithm()
    {
        std::vector<T> &arr = BaseSort<T>::array();        
        buildMaxHeap();
        for(; heap_size > 0; --heap_size)
        {
            std::swap(arr[0], arr[heap_size]);
            maxHeapify(0);
        }

    }
    
}

#endif	/* HEAPSORT_H */

