
#ifndef INSERTIONSORT_H
#define	INSERTIONSORT_H

#include <utility>

#include "BaseSort.h"

namespace core {
    template<typename T>
    class InsertionSort : public BaseSort<T> {
    public:
        InsertionSort(const std::vector<T>& data);
        virtual const std::string info() const;
    protected:
        virtual void sort_algorithm();
    private:

    };
    
    template<typename T>
    const std::string InsertionSort<T>::info() const
    {
        return "Insertion sort, is a simple sorting algorithm "
               "that builds the final sorted array (or list) one item at a time. "
               "It is much less efficient on large lists than more advanced algorithms "
               "such as quicksort, heapsort, or merge sort.\n"
               "Worst case performance: О(n2) comparisons, swaps\n"
               "Best case performance: O(n) comparisons, O(1) swaps\n"
               "Average case performance: О(n2) comparisons, swaps\n"
               "Worst case space complexity: О(n) total, O(1) auxiliary\n";
    }

    template<typename T>
    InsertionSort<T>::InsertionSort(const std::vector<T>& data)
        : BaseSort<T>::BaseSort(data)
    {
        // empty
    }
    
    template<typename T>
    void InsertionSort<T>::sort_algorithm()
    {
        std::vector<T> &arr = InsertionSort<T>::array();
        int i,j;
        
        for(j = 1; j < arr.size(); ++j)
        {
            T key = arr[j];
            i = j - 1;
            while(i >= 0 && BaseSort<T>::compare()(key, arr[i]))
            {
                arr[i+1] = arr[i];
                --i;
            }
            arr[i+1] = key;
        }
    }

}

#endif	/* INSERTIONSORT_H */

