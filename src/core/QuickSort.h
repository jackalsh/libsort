/* 
 * File:   QuickSort.h
 * Author: Olga
 *
 * Created on 18 октября 2014 г., 13:48
 */

#ifndef QUICKSORT_H
#define	QUICKSORT_H

#include <utility>

#include "BaseSort.h"

namespace core {
    template<typename T>
    class QuickSort : public BaseSort<T> {
    public:
        QuickSort(const std::vector<T>& data);
        virtual const std::string info() const;
    protected:
        virtual void sort_algorithm();
        virtual int partition(int p, int r);
        void rec_sort(int p, int r);        
    private:

    };
    
    template<typename T>
    const std::string QuickSort<T>::info() const
    {
        return "Quick sort";
    }

    template<typename T>
    QuickSort<T>::QuickSort(const std::vector<T>& data)
        : BaseSort<T>::BaseSort(data)
    {
        // empty
    }
    
    template<typename T>
    int QuickSort<T>::partition(int p, int r)
    {
        std::vector<T> &arr = QuickSort<T>::array();
        
        const T x = arr[r];
        
        int i = p - 1;
        for(int j = p; j <= r - 1; ++j)
            if(BaseSort<T>::compare()(arr[j], x))
            {
                ++i;
                std::swap(arr[i], arr[j]);
            }
        std::swap(arr[i+1], arr[r]);
        return i + 1;
    }
    
    template<typename T>
    void QuickSort<T>::rec_sort(int p, int r)
    {     
        if( p < r )
        {
            int q = partition(p,r);
            rec_sort(p, q - 1);
            rec_sort(q + 1, r);
        }
    }
    
    template<typename T>
    void QuickSort<T>::sort_algorithm()
    {
        std::vector<T> &arr = QuickSort<T>::array();
        rec_sort(0, arr.size() - 1);
    }
    
}

#endif	/* QUICKSORT_H */

