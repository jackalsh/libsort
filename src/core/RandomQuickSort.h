
#include <iostream>

extern "C"
{
#include "../utils/mt64/mt64.h"
}

#include "QuickSort.h"

#ifndef RANDOMQUICKSORT_H
#define	RANDOMQUICKSORT_H

namespace core {

    template <typename T>
    class RandomQuickSort : public QuickSort<T>
    {
    public:
        RandomQuickSort(const std::vector<T>& data);
    protected:
        void sort_algorithm();
        virtual int partition(int p, int r);
        
    };

    template<typename T>
    RandomQuickSort<T>::RandomQuickSort(const std::vector<T>& data)
        : QuickSort<T>::QuickSort(data)
    {
        // empty
    }
    
    template<typename T>
    int RandomQuickSort<T>::partition(int p, int r)
    {
        std::vector<T> &arr = QuickSort<T>::array();
        
        unsigned long long index = p + genrand64_int64() % (r - p);
        unsigned long long last = r;
        
        std::swap(arr[index], arr[last]);
        QuickSort<T>::partition(p, r);
    }
    
    template<typename T>
    void RandomQuickSort<T>::sort_algorithm()
    {
        std::vector<T> &arr = QuickSort<T>::array();
        QuickSort<T>::rec_sort(0, arr.size() - 1);
    }
    
}

#endif	/* RANDOMQUICKSORT_H */

