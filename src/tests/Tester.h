
#ifndef TESTER_H
#define	TESTER_H

#include "../core/BaseSort.h"

namespace tests
{
    template<typename T>
    bool isSorted(const core::BaseSort<T> &sorter)
    {
        auto compare = sorter.compare();
        auto array = sorter.result();
        
        
        for(unsigned int index = 0; index < array.size() - 1; ++index)
            if(!compare(array[index], array[index+1]))
                return false;
            
        return true;
    }
    
    class Tester;
}

#endif	/* TESTER_H */
