

// For ANSI C generator
#include <time.h>
#include <stdlib.h>

// For Marseene Twister 64
extern "C"
{
#include "mt64/mt64.h"
}

#include "utils.h"

using namespace utils;

DataGenerator::DataGenerator()
{
    // Setting default values. Should be redefinned by user
    setSize(100);
    setMaxValue(100);
    setMinValue(0);
    
    // Set seed
    srand(time(NULL));
    init_genrand64(time(NULL));
}

void DataGenerator::setSize(unsigned int size)
{
    array_size = size;
}

void DataGenerator::setMaxValue(unsigned int max)
{
    array_max_value = max;
}

void DataGenerator::setMinValue(unsigned int min)
{
    array_min_value = min;
}

unsigned int DataGenerator::getSize() const
{
    return array_size;
}

unsigned int DataGenerator::getMaxValue() const
{
    return array_max_value;
}

unsigned int DataGenerator::getMinValue() const
{
    return array_min_value;
}

const std::vector<int> DataGenerator::randomANSI_C()
{
    std::vector<int> data;

    for (unsigned int index = 0; index < getSize(); ++index)
    {
        auto value = getMinValue() + rand() % getMaxValue();
        data.push_back(value);
    }
    
    return data;
}

const std::vector<int> DataGenerator::randomMT64()
{
    std::vector<int> data;
    for (unsigned int index = 0; index < getSize(); ++index)
    {
        auto value = getMinValue() + genrand64_int64() % getMaxValue();
        data.push_back(value);
    }
    return data;
}

const std::vector<int> DataGenerator::userInput()
{
    std::vector<int> data;
    int value;
    for (unsigned int index = 0; index < getSize(); ++index)
    {
        std::cin >> value;
        data.push_back(value);
    }
    return data;
}

const std::vector<int> DataGenerator::userPointer(const int *arr)
{
    std::vector<int> data;
    for (unsigned int index = 0; index < getSize(); ++index)
        data.push_back(arr[index]);
    return data;
}