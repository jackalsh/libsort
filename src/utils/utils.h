
#ifndef UTILS_H
#define	UTILS_H

#include <iostream>
#include <vector>

namespace utils
{
    class DataGenerator
    {
    public:
        DataGenerator();
        
        void setSize(unsigned int size);
        void setMaxValue(unsigned int max);
        void setMinValue(unsigned int min);
        unsigned int getSize() const;
        unsigned int getMaxValue() const;
        unsigned int getMinValue() const;
        
        const std::vector<int> randomANSI_C();
        const std::vector<int> randomMT64();
        const std::vector<int> userInput();
        const std::vector<int> userPointer(const int *arr);
    private:
        unsigned int array_size;
        unsigned int array_max_value;
        unsigned int array_min_value;
        
    };
}

// Handy class to output vectors to stream.
template<typename T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T> array)
{
    for (auto element : array)
        stream << element << " ";
    return stream;
}

#endif	/* UTILS_H */

